# MovieCleansing
Simple python script to batch delete watched movies in Kodi. This is a CLI interface only, when running `MovieCleansing.py` the user is prompt with each movies on the "watched" status in Kodi and can :

- keep it (the movie is added to a whitelist and the user is never prompt again)
- Delete it (note that a manual update of the kodi database is required, use the add-on Watchdog to update it automatically on filesystem changes)
- decide later (the movie is not deleted, but not added to the whitelist and the user will be prompt again later on)

**Usage:**

MovieCleansing.py [-h] [-c] [-d] [-r] [-v]

**Arguments:**

  -h, --help     show this help message and exit

  -c, --clean    Prompt to keep or delete watched movies.

  -d, --display  Display the movies in the whitelist

  -r, --remove   Remove movie from the whitelist

  -v, --verbose  Increase output verbosity


# Installation

**Python**

Python version 3.6 or above

_On Linux:_
```bash
python3 --version
```

**Clone the repository**

Git is required
```bash
git --version
```

Clone it
```bash
git clone git@bitbucket.org:aymericdeschard/movie_cleansing.git
```


**Executable**

Allows execution of entry point
```bash
chmod +x MovieCleansing.py
```

**Bash shortcut**

If you want to access it directly from the terminal (Linux and MacOsX) with the command `clean_movies`:

```bash
INSTALL_DIR="<the full path to the installation directory>"
printf "# clean_movies\nalias clean_movies='${INSTALL_DIR}/MovieCleansing.py'" >> ~/.bash_aliases
```

**Update**

```bash
git pull
```



# License and EULA
Unmodified [MIT license](https://opensource.org/licenses/MIT)

See `LICENSE.md`

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Contributing

I welcome any suggestion, corrections or improvements via push requests or email.

Please do report any bugs with:

- the error message,
- the log,
- the steps to reproduce it,
- the OS type and version
